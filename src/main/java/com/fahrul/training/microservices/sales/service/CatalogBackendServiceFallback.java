package com.fahrul.training.microservices.sales.service;

import org.springframework.stereotype.Component;

import com.fahrul.training.microservices.sales.dto.ProductDto;

import lombok.extern.slf4j.Slf4j;

@Component @Slf4j
public class CatalogBackendServiceFallback implements CatalogBackendService{
    @Override
    public ProductDto findProductById(String id) {
        log.info("Fallback find product by id : {}", id);
        return null;
    }
}
