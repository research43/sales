package com.fahrul.training.microservices.sales.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ProductDto {
    private String code;
    private String name;
    private BigDecimal price;
    private String backendSource;
}

