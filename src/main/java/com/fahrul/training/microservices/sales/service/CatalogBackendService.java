package com.fahrul.training.microservices.sales.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.fahrul.training.microservices.sales.dto.ProductDto;

@FeignClient(name = "catalog", fallback = CatalogBackendServiceFallback.class)
public interface CatalogBackendService {
    @GetMapping("/api/product/{id}")
    ProductDto findProductById(@PathVariable String id);
}



