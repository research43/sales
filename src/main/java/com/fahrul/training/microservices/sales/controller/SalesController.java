package com.fahrul.training.microservices.sales.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fahrul.training.microservices.sales.dao.SalesDao;
import com.fahrul.training.microservices.sales.dto.ProductDto;
import com.fahrul.training.microservices.sales.entity.Sales;
import com.fahrul.training.microservices.sales.entity.SalesDetail;
import com.fahrul.training.microservices.sales.service.CatalogBackendService;


@RestController
@RequestMapping("/api/sales")
public class SalesController {

    @Autowired private SalesDao salesDao;
    @Autowired private CatalogBackendService catalogBackendService;

    @GetMapping("/{id}")
    public Sales findById(@PathVariable String id) {
        Sales sales = salesDao.findById(id).orElse(null);

        if (sales != null) {
            for (SalesDetail sd : sales.getSalesDetails()) {
                String productCode = sd.getProductCode();
                ProductDto product = catalogBackendService.findProductById(productCode);
                sd.setProductDetail(product);
            }
        }

        return sales;
    }

}
