package com.fahrul.training.microservices.sales.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity @Data
public class Customer {

    @Id @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotEmpty @NotNull @Email
    @Size(min = 3, max = 100)
    private String email;

    @NotEmpty @NotNull
    @Size(min = 2, max = 255)
    private String fullname;
}
