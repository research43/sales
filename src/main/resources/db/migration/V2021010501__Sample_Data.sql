insert into customer (id,email,fullname) values ('d2c59d80-50f4-4fe7-912e-d9eb5415be58','fahrul.mundjid@gmail.com','Fahrul Mundjid');
insert into customer (id,email,fullname) values ('d2c59d80-50f4-4fe7-912e-d9eb5415be59','abc@yumail.com','ABC yumail');

insert into sales (id,id_customer,sales_number,transaction_time) values ('d2c59d80-50f4-4fe7-912e-d9eb5415bd58','d2c59d80-50f4-4fe7-912e-d9eb5415be58','50','2021-01-05');
insert into sales (id,id_customer,sales_number,transaction_time) values ('d2c59d80-50f4-4fe7-912e-d9eb5415bd59','d2c59d80-50f4-4fe7-912e-d9eb5415be59','50','2021-01-05');

insert into sales_detail (id,id_sales,product_code,quantity,unit_price) values ('d2c59d80-50f4-4fe7-912e-d9eb5415qd58','d2c59d80-50f4-4fe7-912e-d9eb5415bd58','p001','100','20000');
insert into sales_detail (id,id_sales,product_code,quantity,unit_price) values ('d2c59d80-50f4-4fe7-912e-d9eb5415qd59','d2c59d80-50f4-4fe7-912e-d9eb5415bd58','p001','150','210000');
